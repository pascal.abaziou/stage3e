#language: fr
# -- FILE: features/example.feature
Fonctionnalité: Showing off behave

  Scénario: Exécuter un simple test
    Etant donné we have behave installed
     Quand we implement 5 tests
     Alors behave will test them for us!
