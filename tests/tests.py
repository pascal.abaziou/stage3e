import unittest

from elementary.fizz_buzz import checkio


class TestBasic(unittest.TestCase):

    def test_running(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_running2(self):
        self.assertEqual('Fizz Buzz',  checkio(15))

    def test_running3(self):
        self.assertEqual('Fizz', checkio(6))

    def test_running4(self):
        self.assertEqual('Buzz', checkio(5))

    def test_running5(self):
        self.assertEqual('7', checkio(7))



if __name__ == '__main__':
    unittest.main()
